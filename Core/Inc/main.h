/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define OE_TXS0102_Pin GPIO_PIN_13
#define OE_TXS0102_GPIO_Port GPIOC
#define EN_PRESS_TEMP_Pin GPIO_PIN_0
#define EN_PRESS_TEMP_GPIO_Port GPIOB
#define ADC_MIC_Pin GPIO_PIN_1
#define ADC_MIC_GPIO_Port GPIOB
#define EN_MIC_Pin GPIO_PIN_2
#define EN_MIC_GPIO_Port GPIOB
#define EN_TEMP_Pin GPIO_PIN_10
#define EN_TEMP_GPIO_Port GPIOB
#define EN_BAL_BT_Pin GPIO_PIN_11
#define EN_BAL_BT_GPIO_Port GPIOB
#define REED_Pin GPIO_PIN_12
#define REED_GPIO_Port GPIOB
#define MODEM_POWERKEY_Pin GPIO_PIN_11
#define MODEM_POWERKEY_GPIO_Port GPIOA
#define INT1_ACCEL_Pin GPIO_PIN_15
#define INT1_ACCEL_GPIO_Port GPIOA
#define _BURN_P22_Pin GPIO_PIN_5
#define _BURN_P22_GPIO_Port GPIOB
#define DROPOUT_P24_Pin GPIO_PIN_6
#define DROPOUT_P24_GPIO_Port GPIOB
#define INT2_ACCEL_Pin GPIO_PIN_7
#define INT2_ACCEL_GPIO_Port GPIOB
#define EN_GPS_Pin GPIO_PIN_8
#define EN_GPS_GPIO_Port GPIOB
#define EN_GPRS_Pin GPIO_PIN_9
#define EN_GPRS_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
